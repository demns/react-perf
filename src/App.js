import React, { Component } from 'react';
import ListItem from './listItem';
import Perf from 'react-addons-perf';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arr: this.arrayGenerator(10001)
    }
    this.resetMultiplier = this.resetMultiplier.bind(this);
  }

  arrayGenerator(length) {
    return [...Array(length).keys()];
  }

  componentDidUpdate() {
    Perf.stop();
    Perf.printInclusive();
    Perf.printExclusive();
    console.log("this is wasted: ")
    Perf.printWasted();
    Perf.printOperations();
  }

  resetMultiplier() {
    Perf.start();
    this.setState(() => ({
      arr: [...this.state.arr.slice(0, this.state.arr.length - 1), -1]
    }));
  }

  render() {
    return (
      <div className="App">
        <button onClick={this.resetMultiplier}>Press to make arr[10000] === -1</button>
        <ul>
          {this.state.arr.map(i => <ListItem key={i} text={i} />)}
        </ul>
      </div>
    );
  }
}

export default App